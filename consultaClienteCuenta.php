<?php
include_once('header.php');
//session_start();
?>
<style>
    table{
        white-space: nowrap; 
    }
    .ellipsis{
        height: 20px;
        width: 100px;
        overflow: hidden;
        text-overflow: ellipsis;

    }
</style>
<center>
    <h1>CONSULTA DE CUENTAS POR CLIENTE</h1>
</center>
<div>
    <input id="nomUser" class="nomUser" value="<?php echo $_SESSION['fullname'] ?>" type="hidden"/>
    <input type="hidden" id="tipoNotificacion" value="">
    <input type="hidden" id="tipoNotificacionUsuaio" value="">
</div>
<div class="panel">
    <div class="panel-body">
        <div class="row">
            <input id="idEmp" class="idEmp" value="" type="hidden"/>
            <div class="col-md-4">
                <label class="label" style="color: gray">Nombre o Apellido</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input class="form-control input-sm" id="fullNameCliente" type="text" value="" placeholder="Nombres o Apellidos" required="true"/>
                    <span class="input-group-addon btn-danger hide" id="limpiarEmp" style="color:white"><i class="fa fa-times"></i></span>
                </div>
            </div>
            <div class="col-md-2">
                <label class="label" style="color: gray"> </label>
                <button class="btn btn-block btn-sm btn-success" id="buscarReporte">Buscar</button><br>
                <button class="btn btn-block btn-sm btn-info hide" id="limpiarReporte">Limpiar</button>
            </div>
        </div>
        <br>

        <div class="table-responsive">
            <table id="tblReporte" class="table table-condensed" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>N°</th>
                        <th>Cédula</th>
                        <th>Nombres</th>
                        <th>Cuenta</th>
                        <th>Estado</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

</div>




</section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->

<!--main content end-->
<!--footer start-->
<footer class="site-footer">
    <div class="text-center">
        2019 - SisteCoop
    </div>
</footer>
<!--footer end-->
</section>


<!-- js placed at the end of the document so the pages load faster -->
<script src="assets/js/jquery-3.2.1.min.js"></script>    



<script src="assets/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


<!--common script for all pages-->
<script src="Plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
<script src="assets/js/common-scripts.js"></script>

<!--external script for all pages-->

<script src="Plugins/SweetAlert2/js/sweetalert2.min.js" type="text/javascript"></script>

<script src="Plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="Plugins/DataTables-1.10.15/media/js/dataTables.uikit.min.js" type="text/javascript"></script>




<script src="Plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="Plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.bootstrap.min.js" type="text/javascript"></script>
<script src="Plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="Plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js" type="text/javascript"></script>
<script src="Plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.colVis.min.js" type="text/javascript"></script>

<script type="text/javascript" language="javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js">
</script>
<script type="text/javascript" language="javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js">
</script>
<script type="text/javascript" language="javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js">
</script>

<script src="Plugins/full_calendar/moment.min.js" type="text/javascript"></script>


<link href="Plugins/DataTables-1.10.15/extensions/Buttons/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="Plugins/DataTables-1.10.15/extensions/Buttons/css/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>


<script src="Plugins/DataTables-1.10.15/inicializar.js" type="text/javascript"></script>
<!--end external script for all pages-->

<script src="funciones/header/header.js" type="text/javascript"></script>
<script src="Plugins/DataTables-1.10.15/media/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<!--<script src="Plugins/JSpdf/jspdf.min.js" type="text/javascript"></script>-->
<script src="Plugins/JSpdf/jspdf.debug.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.2/jspdf.plugin.autotable.js"></script>
<script src="funciones/consultaClienteCuenta/consultaClienteCuenta.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {

//        $(".peticionesT").addClass("active");
//        $(".mispeticiones").addClass("active");


    });

</script>


</body>
</html>


