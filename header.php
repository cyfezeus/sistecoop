<?php
include_once('./funciones/conexion/conexion.php');
$conexion = new conexion();
session_start();
if ($_SESSION['nom_usuario'] == null) {
    header('location:index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Dashboard">
        <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

        <title>Sistema Cooperativa 1.0</title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <!-- Optional theme -->
        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">

        <!--external css-->
        <link href="Plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/> 
        <link href="Plugins/full_calendar/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
        <link href="Plugins/SweetAlert2/css/sweetalert2.min.css" rel="stylesheet" type="text/css"/>
        <link href="Plugins/DataTables-1.10.15/media/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>


        <!-- Custom styles for this template -->
        <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="assets/css/style.css" rel="stylesheet">
        <link href="assets/css/style-responsive.css" rel="stylesheet">
        <link href="assets/css/jquery.orgchart.min.css" rel="stylesheet" type="text/css"/>

        <link rel="icon" type="image/png" sizes="256x256" href="assets/img/camaron.png">

        <style>
            .fc-day-grid-container.fc-scroller { 
                height: auto!important; 
                overflow-y: auto; 
            }

            .fc-time-grid-container.fc-scroller { 
                height: auto!important; 
                overflow-y: auto; 
            }
            div > .fc-scroller {
                min-height: 100px !important;	 
                height: auto!important; 
                overflow-y: auto; 
            }
        </style>

    </head>

    <body>

        <section id="container" >
            <!-- **********************************************************************************************************************************************************
            TOP BAR CONTENT & NOTIFICATIONS
            *********************************************************************************************************************************************************** -->
            <!--header start-->
            <header class="header black-bg">
                <input id="vacacionesId" class="vacacionesId"  type="hidden"/>
                <div class="sidebar-toggle-box">
                    <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
                </div>
                <!--logo start-->
                <a href="index.html" class="logo"><b>Siste</b><b style="color:#b4ffb4">Coop</b></a>
                <!--logo end-->

                <div class="top-menu">
                    <ul class="nav pull-right top-menu">
                        <li><a class="logout btn btn-theme03" href="funciones/conexion/cerrar.php">Logout</a></li>
                    </ul>
                </div>
            </header>
            <!--header end-->

            <!-- **********************************************************************************************************************************************************
            MAIN SIDEBAR MENU
            *********************************************************************************************************************************************************** -->
            <!--sidebar start-->
            <aside>
                <div id="sidebar"  class="nav-collapse" style="z-index: 1000">
                    <!-- sidebar menu start-->
                    <ul class="sidebar-menu" id="nav-accordion" style="min-height:74vh">

                        <p class="centered"><a><img src="assets/img/avatar5.png" class="img-circle" width="60"></a></p>
                        <h5 class="centered"><?php echo $_SESSION['fullname'] ?></h5>
                        <li>
                            <a class="aguaje" href="inicio.php">
                                <i class="fa fa-dashboard"></i>
                                <span>Inicio</span>
                            </a>
                        </li>
                        <li>
                            <a class="aguaje" href="consultaClienteCuenta.php">
                                <i class="fa fa-angle-right"></i>
                                <span>Consulta de Cuentas</span>
                            </a>
                        </li>
                       <li>
                            <a class="aguaje" href="consultaTransaccion.php">
                                <i class="fa fa-angle-right"></i>
                                <span>Consulta de Transaccion</span>
                            </a>
                        </li>
                       <li>
                            <a class="aguaje" href="consultaUsuario.php">
                                <i class="fa fa-angle-right"></i>
                                <span>Consulta de Usuarios</span>
                            </a>
                        </li>    
                       
                    </ul>
                    <!-- sidebar menu end-->
                </div>
            </aside>
            <!--sidebar end-->

            <!-- **********************************************************************************************************************************************************
            MAIN CONTENT
            *********************************************************************************************************************************************************** -->
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper site-min-height">

