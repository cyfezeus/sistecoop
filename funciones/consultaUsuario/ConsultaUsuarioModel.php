<?php

require_once '../conexion/conexion.php';

class ConsultaUsuarioModel extends conexion {

    public function getReporte($username) {
        $username = is_null($username) ? "" : strtoupper($username);

        $sentences = "SELECT u.usuario_id as usuarioCodigo,
                                u.usuario_username as usuarioNombre,
                            u.usuario_email as usuarioMail
                     FROM usuario u";
        if ($username != "") {
            $sentences = $sentences . " WHERE  u.usuario_username like '%$username%' ";
        }

        $result = $this->realizarConsulta($sentences);
        return $result;
  }

}
