<?php

require_once '../conexion/conexion.php';

class ConsultaTransaccionModel extends conexion {

    public function getReporte($tipoTransaccionCofigo) {
                
        $sentences = "select a.transaccion_codigo as transaccionCodigo,
                             a.transaccion_fecha as transaccionFecha,
                            b.tip_tra_codigo as tipoTransaccionCodigo,
                            b.tip_tra_nombre as tipoTransaccionNombre,
                            a.transaccion_monto as transaccionMonto,
                            a.cliente_codigo as clienteCodigo,
                            concat(d.cliente_apellido,' ',d.cliente_nombre) as clienteNombreCompleto,
                            a.cuenta_codigo as cuentaCodigo,
                            c.cuenta_numero as cuentaNumero
                     from transaccion a, 
                              tipo_transaccion b,
                          cuenta c,
                          cliente d
                     where a.tip_tra_codigo = b.tip_tra_codigo
                     and	  a.cliente_codigo = d.cliente_codigo
                     and	  a.cuenta_codigo = c.cuenta_codigo";
        if ($tipoTransaccionCofigo != null) {
            $sentences = $sentences . " and	  b.tip_tra_codigo = $tipoTransaccionCofigo ";
        }
        
        $result = $this->realizarConsulta($sentences);
               
        return $result;
  }
  
    public function getTipoTransaccion() {
        
        $sentences = "select tip_tra_codigo as tipoTransaccionCodigo,
                             tip_tra_nombre as tipoTransaccionNombre
                     from tipo_transaccion";
        
        $result = $this->realizarConsulta($sentences);
        return $result;
  }

}
