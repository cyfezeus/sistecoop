<?php

header("Content-Type: application/json;charset=utf-8");
require_once 'ConsultaTransaccionModel.php';
//session_start();
$reporte = new ConsultaTransaccionModel();

if ($_POST['opcion'] == 'consultaTransaccion') {
       echo $reporte->respuestaJson($reporte->getReporte($_POST['tipoTransaccionCofigo']));
}

if ($_POST['opcion'] == 'consultaTipoTransaccion') {
       echo $reporte->respuestaJson($reporte->getTipoTransaccion());
}
