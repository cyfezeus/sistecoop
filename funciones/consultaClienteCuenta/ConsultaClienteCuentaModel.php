<?php

require_once '../conexion/conexion.php';

class ReporteEvento extends conexion {

    public function getReporte($fullNameCliente) {
        $fullNameCliente = is_null($fullNameCliente) ? "" : strtoupper($fullNameCliente);

        $sentences = "select c.cliente_documento as cedula,
                             concat(c.cliente_nombre,' ',c.cliente_apellido) as fullName,
                             t.cuenta_numero as cuenta,
                             e.estado_nombre as estado
                       from  cliente c,
                             cuenta t,
                             cliente_cuenta ct,
                             estado e
                       where c.cliente_codigo = ct.cliente_codigo
                       and   t.cuenta_codigo = ct.cuenta_codigo
                       and   e.estado_codigo = ct.estado_codigo";
        if ($fullNameCliente != "") {
            $sentences = $sentences . " and   concat(c.cliente_nombre,' ',c.cliente_apellido) like '%$fullNameCliente%' ";
        }

        $result = $this->realizarConsulta($sentences);
        return $result;
  }

}
