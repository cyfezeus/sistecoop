<?php

require_once '../conexion/conexion.php';

class IndexModel extends conexion {

    public function iniciarSesion($user, $pass) {

        if (!empty($user) && !empty($pass)) {
            
            $user = strtoupper($user);
                       
            $dato = $this->realizarConsulta("           
                select u.usuario_id,
                u.usuario_username,
                u.usuario_password,
                e.empleado_codigo,
                e.empleado_nombre,
                e.empleado_apellido as lastname,
                concat(e.empleado_apellido,' ',e.empleado_nombre) as fullname
                from usuario u, empleado e
                where u.empleado_codigo = e.empleado_codigo
                and	  u.usuario_username = '$user'
                and	  u.usuario_password = '$pass';
             ");
                     
            if ($dato != null) {
                
                
                if ($dato[0]['usuario_password'] == $pass) {
                    
                    //abilita el posteo
                    /*$posteo = FALSE;
                    $excepcionPersonas = $this->realizarConsulta("SELECT *
                                                FROM asignar_personal_excepciones 
                                                where id_estado=1 and id_empleado=".$dato[0]['id_empleado'].";");
                    $roles = $this->realizarConsulta("SELECT * 
                                                FROM jerarquias 
                                                where id_rol_padre=".$dato[0]['id_rol'].";");
                    if (count($excepcionPersonas) || count($roles)) {
                        $posteo = TRUE;
                    }*///fin abilita el posteo

                    $id_usuario = $dato[0]['usuario_id'];
                    $id_empleado=$dato[0]['empleado_codigo'];
                    $nom_usuario = $dato[0]['lastname'];
                    $fullname = $dato[0]['fullname'];
                    
                    $_SESSION['id_usuario'] = $id_usuario;
                    $_SESSION['id_empleado'] = $id_empleado;
                    $_SESSION['nom_usuario'] = $nom_usuario;
                    $_SESSION['fullname'] = $fullname;
                    $_SESSION['posteo'] = FALSE;

                    $this->mensajes('success', 'inicio.php');
                } else {
                    $this->mensajes("error", "Usuario y/o contraseña incorrecta!");
                }
             
            } else {
                $this->mensajes('error', 'Usuario y/o contraseña incorrecta!');
            }
        } else {
            $this->mensajes('error', 'Debe llenar los campos!');
        }
    }

}
