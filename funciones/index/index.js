$(document).ready(function () {
    $(function () {
        $('#formulario').submit(function () {
            $.ajax({
                type: 'POST',
                url: 'funciones/index/indexControlador.php',// El script a dónde se realizará la petición.
                data: $('#formulario').serialize(),// Adjuntar los campos del formulario enviado.
                success: function (data){
                    if(data['data']['tipo'] == 'success'){
                        window.location = data['data']['texto'];
                    }else{
                        swal("Mensaje", data['data']['texto'], data['data']['tipo']);
                    }
                }
            });//fin ajax
            return false;// Evitar ejecutar el submit del formulario.
        });//fin formulario
    });//fin function  
});//fin document


