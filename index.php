<html>
    <head>
        <meta charset="utf-8">
        <title>Entrar</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">


        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>

        <link href="Plugins/SweetAlert2/css/sweetalert2.min.css" rel="stylesheet" type="text/css"/>
        <link rel="icon" type="image/png" sizes="256x256" href="assets/img/SISTECOOP_VERSION_2.png">
        <style type="text/css">
            body {
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #f5f5f5;
                background-image: url(assets/img/contabilidad.jpg);
                background-size: cover;
            }

            .form-signin {
                max-width: 350px;
                padding: 19px 29px 29px;
                margin: 0 auto 20px;
                background-color: #fff;
                border: 1px solid #e5e5e5;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            }


        </style>
    </head>
    <body>

        <div class="container-fluid">
            <form id="formulario" name="formulario" method="post" class="form-signin">
                <img src="assets/img/SISTECOOP_VERSION_2.png" class="img-rounded" 
                       style="height: 10vh;width: 100%;margin-top: 10%;">
                <center><h2 class="form-signin-heading">Bienvenido</h2></center>
                <input type="hidden" name="opcion" value="iniciar">
                <div class="row" style="margin-top: 8%;">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input autocomplete="off" class="form-control input-lg" type="text" id="user" name="user" placeholder="Usuario" required="true"/>
                        </div><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-unlock-alt"></i></span>
                            <input autocomplete="off" class="form-control input-lg" type="password" id="pass" name="pass" placeholder="Contraseña" required="true"/>
                        </div><br>
                    </div>
                </div>
                <button class="form-control btn btn-warning input-lg" type="submit"><h4>Iniciar</h4></button>
                <p>&nbsp;</p>
            </form>
        </div> <!-- /container -->


        <script src="assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
        <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="funciones/index/index.js" type="text/javascript"></script>

        <script src="Plugins/SweetAlert2/js/sweetalert2.min.js" type="text/javascript"></script>

    </body>  
</html>